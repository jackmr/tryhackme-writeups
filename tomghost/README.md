# TryHackMe tomghost writeup
* https://tryhackme.com/room/tomghost

## Scanning
* port 22/tcp SSH
* port 53/tcp tcpwrapped
* port 8009/tcp ajp13 (Apache Jserv)
* port 8080/tcp Appache Tomcat 9.0.30

## Initial Access
* Searched in Metasploit for AJP
* Found module : `admin/http/tomcat_ghostcat`
* Got me a username:password in the description : `skyfuck:8730281lkjlkjdqlksalks`
* Tried SSH with it : Works!
* First flag in `/home/merlin/user.txt`

## Privelege Escalation
* Got linpeas.sh : no good result
* SUID files : nope
* sudo -l : nope
* Interesting files in `/home/skyfuck`
    * `credential.gpg`
    * `tryhackme.asc`
* Those are a PGP private key and encrypted message
    * `gpg2john tryhackme.asc ­> hashfile`
    * `john --format=gpg --wordlist=rockyou.txt hashfile` : `alexandru`
    * `gpg --import tryhackme.asc` : put `alexandry` as the passphrase
    * `gpg -d credential.gpg`
    * Get : `merlin:asuyusdoiuqoilkda312j31k2j123j1g23g12k3g12kj3gk12jg3k12j3kj123j`
*  SSH as Merlin in the box
* `sudo -l` : `(root : root) NOPASSWD: /usr/bin/zip`
* Check GTFO Bins for Zip : https://gtfobins.github.io/gtfobins/zip/#sudo
* Get root
* Get root flag in `/root/root.txt`

## WIN



