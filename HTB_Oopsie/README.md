# HackTheBox Oopsie 
* Starting Point Box
* https://app.hackthebox.eu/machines/Oopsie

## Loot
* `admin@megacorp.com`

## Not loot, but nice
* 10.10.10.28
* 22/tcp : OpenSSH 7.6p1
* 80/tcp : Apache httpd 2.4.29
    * /.htpasswd
    * /.htaccess
    * /css      -> nothing found there
    * /fonts    -> nothing found there
    * /js       -> nothing found there
    * /themes   -> nothing found there
    * /images
        * 1.jpg
        * 2.jpg
        * 3.jpg
    * /uploads
        * 
    * /cdn-cgi
        * /cdn-cgi/scripts
        * /cdn-cgi/login        -> LOGIN FORM
            * /cdn-cgi/login/script.js
 
* OS : Ubuntu Linux 

## 1 - Scanning
* `nmap -sV -sC -p- 10.10.10.28`
* `nmap -O -A 10.10.10.28`          -> nothing new

## 2 - Web Exploration
* `gobuster dir -u http://10.10.10.28 -w /usr/share/wordlists/dirb/big.txt`
* `gobuster dir -u http://10.10.10.28 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt`
* `gobuster dir -u http://10.10.10.28 -w /usr/share/wordlists/dirb/big.txt -x .bak,.txt`
* Some subdirectory enumeration with gobuster .. 
* Some enumeration with extensions
* `gobuster dir -u http://10.10.10.28/images -w /usr/share/wordlists/dirb/big.txt -x .bak,.txt,.jpg,.png`
* I then looked into the page code with Burp Suite and fond this URL : /cdn-cgi/login/script.js and the /cdn-cgi/login gives a login page
* Could have been found with this also : `gobuster dir -u http://10.10.10.28/cdn-cgi -w /usr/share/wordlists/dirb/big.txt -x .bak,.txt,.php`
* `nikto -host http://10.10.10.28/cdn-cgi/login/index.php`

## 3 - Web Exploitation
* `sqlmap -r request  -t admin --dump` (request being an export from Burp)
    * No result
* (CAN'T, NO ERROR MSG) `hydra -l admin -P wordlist 10.10.10.28 http-post-form "/cdn-cgi/login/index.php:username=^USER^&password=^PASS^"`

## 4 - SSH Exploitation
* `searchsploit openssh 7.6` : founds a user enum vulnerability
    * says all usernames are valid .. 


