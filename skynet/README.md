# TryHackMe Skynet Room
* https://tryhackme.com/room/skynet
* very approx writeup

## Scanning and Enumeration
* Nmap Scan 
    * ssh port 22
    * http port 80
    * pop3 port 110
    * smbd port 139
    * imap port 143
    * smbd port 445
    * Host = Linux
* Gobuster :
    * /squirrelmail => LOGIN PAGE
* Smb Enumeration permits anonymous to anonymous share
* Some back and forth between the website and the SMB shares with different users
* `http://10.10.12.51/45kra24zxs28v3yd/administrator/`

## Creds
* HTTP: `milesdyson:cyborg007haloterminator`
* SMB: `milesdyson:)s{A&2Z=F^n_E.B``
* DB: `root:password123` (not used)


## Initial Access
* LFI found with searchsploit andfor Cuppa CMS 
* `http://10.10.12.51/45kra24zxs28v3yd/administrator/alerts/alertConfigField.php?urlConfig=../../../../../../../../../etc/passwd`

## PrivEsc to root
* There is a tar script running every few minutes and creating an archive 
